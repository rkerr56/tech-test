/**
 * Function to create N equal sized arrays from a provided array of integers.
 * Where the array cannot be equally divided, the final group should contain the remainder.
 * 
 * @param {Array} elements The elements to be split
 * @param {Number} groups The number of groups to split elements into
 * @return {Array} An array of size = groups containing the split elements, or empty
 */
function groupArrayElements(elements, groups) {

    const elementsSize = elements.length;
    const groupedArrayElements = [];

    // If group size is greater than array size or the array is empty, return initial array
    if (groups > elementsSize || elementsSize == 0) {
        return elements;
    }

    // If size of array == expected group size, the returned groups will contain a single item since they can be evenly divided
    if (elementsSize == groups) {

        elements.map(item => {
            groupedArrayElements.push([item]);
        })

        return groupedArrayElements;
    }

    //  Calculate how many items each group should hold
    //  If provided group size is > half the overall array size, round down, otherwise round up.
    const itemsPerGroup = (groups > Math.ceil(elementsSize / 2)) ? Math.floor(elementsSize / groups) : Math.ceil(elementsSize / groups);

    // Iterate elements to populate each group using itemsPerGroup as the index modifier
    for (i = 0; i < elementsSize; i += itemsPerGroup) {
        // First populate groups up to (groups - 1) since the last group will handle remainders
        if (groupedArrayElements.length < groups - 1) {
            // Add itemsPerGroup to the index to ensure we get the correct end indice for each group
            groupedArrayElements.push(elements.slice(i, (i + itemsPerGroup)));
        } else {
            // This handles the last group, so push all remaining elements
            groupedArrayElements.push(elements.slice(i, elementsSize));
            break;
        }
    }

    return groupedArrayElements;
}

/**
 * Example execution
 */
// console.log(groupArrayElements([], 5));
// console.log(groupArrayElements([1, 2, 3, 4], 2));
console.log(groupArrayElements([1, 2, 3, 4, 5], 3));
// console.log(groupArrayElements([1, 2, 3, 4, 5], 4));
// console.log(groupArrayElements([1, 2], 4));
// console.log(groupArrayElements([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13], 4));
// console.log(groupArrayElements([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3));
// console.log(groupArrayElements([], 0));