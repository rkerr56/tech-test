# Tech Test
This repository contains the code for the tech test. It also contains a word document containing further details on the implementation, and some example test cases considered when ensuring the functionality works as expected. 

The code requires node to be installed if not already: https://nodejs.org/en/

## Running the code
1. Clone this repository
2. Go to the cloned repository on your local file system
3. On the same level as the 'twig.js' file, run:
    - node twig.js

This will output the result of the function being called with various parameter combinations. These are output via a console log which wraps the function call, so the provided examples in the twig.js file can be edited to test different scenarios. The test scenarios are also listed within the attached solution document with more detail on the implementation.

If there are any issues feel free to contact me at ryank979@gmail.com
